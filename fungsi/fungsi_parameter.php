<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    //membuat fungsi
    function perkenalan($nama, $salam)
    {
        echo $salam . ", ";
        echo "perkenalkan, nama saya " . $nama . "<br/>";
        echo "senang berkenalan dengan anda<br/>";
    }
    //memanggil fungsi
    perkenalan("Muhardian", "hi");
    echo "<hr>";
    $saya = "indry";
    $ucapansalam = "selamat pagi";
    //memanggil lagi
    perkenalan($saya, $ucapansalam)
    ?>
</body>

</html>