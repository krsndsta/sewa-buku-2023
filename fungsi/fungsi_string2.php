<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    //membuat fungsi
    function hitungUmur($tahunlahir, $tahunsekarang)
    {
        $umur = $tahunsekarang - $tahunlahir;
        return $umur;
    }
    function perkenalan($nama, $salam = "assalamualaikum")
    {
        echo $salam . ", ";
        echo "perkenalkan, nama saya " . $nama . "<br/>";
        //memanggil fungsi lain
        echo "saya berusia " . hitungUmur(2000, 2020) . "tahun<br/>";
        echo "senang berkenalan dengan anda<br/>";
    }
    //memanggil fungsi perkenalan
    perkenalan("ardianta");
    ?>
</body>

</html>