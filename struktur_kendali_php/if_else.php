<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>kondisi if else</title>
</head>

<body>
    <?php
    $suhu = 20;
    //KONDISI DENGAN MENGGUNAKAN IF ELSE
    if ($suhu <= 20) {
        echo "suhu sejuk";
    } elseif ($suhu > 20 and $suhu <= 27) {
        echo "suhu biasa";
    } else {
        echo "udara panas";
    }
    ?>
</body>

</html>