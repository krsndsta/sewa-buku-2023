<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>if else 2</title>
</head>

<body>
    <?php
    $nama_hari = date("D");
    if ($nama_hari == "Sun")
        echo "minggu";
    elseif ($nama_hari == "Mon")
        echo "senin";
    elseif ($nama_hari == "Tues")
        echo "selasa";
    elseif ($nama_hari == "Wedn")
        echo "rabu";
    elseif ($nama_hari == "Thurs")
        echo "kamis";
    elseif ($nama_hari == "Fri")
        echo "jumat";
    else
        echo "sabtu";
    ?>
</body>

</html>