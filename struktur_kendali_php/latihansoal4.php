<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>latihan soal</title>
</head>

<body>
    <?php
    // Mendefinisikan tiga variabel
    $angka1 = 10;
    $angka2 = 5;
    $angka3 = 2;

    // Melakukan operasi aritmatika
    $hasilTambah = $angka1 + $angka2 + $angka3;
    $hasilKurang = $angka1 - $angka2;
    $hasilKali = $angka1 * $angka3;
    $hasilBagi = $angka2 / $angka3;

    // Menampilkan hasil
    echo "Hasil Penjumlahan: $hasilTambah <br>";
    echo "Hasil Pengurangan: $hasilKurang <br>";
    echo "Hasil Perkalian: $hasilKali <br>";
    echo "Hasil Pembagian: $hasilBagi <br>";
    ?>
</body>

</html>