<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Switch Case 2</title>
</head>

<body>
    <?php
    $kendaraan = "sepeda";
    switch ($kendaraan) {
        case "mobil":
            echo "saya memakai kendaraan mobil ketika berangkat";
            break;
        case "grab":
        case "gojek":
            echo "saya memakai kednaraan ojek online ketika berangkat";
            break;
        case "bus":
            echo "saya memakai kendaraan bus ketika berangkat";
        case "sepeda":
            echo "saya memakai kendaraan sepeda ketika berangkat";
            break;
        default:
            echo "saya hari ini tidak berangkat";
            break;
    }
    ?>
</body>

</html>